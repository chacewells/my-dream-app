import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  username = '';
  canClear = false;

  constructor() { }

  ngOnInit() {
  }

  onTypey() {
    this.canClear = this.username.length > 0;
  }

  onClearInput() {
    this.username = '';
  }

}
